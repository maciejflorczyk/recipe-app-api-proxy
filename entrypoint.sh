#!/bin/sh

set -e #if any of the lines fail, return error to the screen

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

#start nginx service
nginx -g 'daemon off;'